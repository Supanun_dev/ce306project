<!DOCTYPE html>
<html>
	<head>
		<title>Project :: CE306</title>
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<script src="js/jquery.min.js"></script>
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

		<link href='http://fonts.googleapis.com/css?family=Montserrat+Alternates:400,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>

		<script src="js/modernizr.custom.js"></script>
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
			
		<style>
			#openCanvas1, #openCanvas2, #openCanvas3 {
				display: none;
			}
			.og-close {
				top: 10px;
			}
			canvas {
				border:1px solid #d3d3d3;
				background-color: #f1f1f1;
				width: 1030px;
				height: 401px;
			}
		</style>
	</head>

	<body> 



		<div class="header" >	
			<div class="header-top">
				<div class="container">
					<div class="head-top">
						<div class="logo">
							<h1><a href="index.html"><span> G</span>ames <span>C</span>enter<span><img src="images/lgg.png" alt="" style="transform: rotate(20deg); -webkit-transform: rotate(20deg); -ms-transform: rotate(20deg); width: 80px;"></span></a></h1>
						</div>
						
						<div class="top-nav">		
							<span class="menu"><img src="images/menu.png" alt=""> </span>
							<ul>
								<li><a id="uemail"></a></li>
								<li><a id="">PLAYER</a></li>
								<li><a class="color6" href="login.html" >LOG OUT</a></li>
								<div class="clearfix"> </div>
							</ul>
								
							<script>
								$("span.menu").click(function(){
									$(".top-nav ul").slideToggle(500, function(){
									
									});
								});
							</script>

						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
		</div>

	
		<div class="content">
			<div class="container">
				<div class="content-top">
					<h2 class="new">ALL GAMES</h2>
					<div class="wrap">	
						<div class="main">
							<ul id="og-grid" class="og-grid" style="padding-bottom: 0px;">
								<li id="li1" style="transition: height 350ms ease 0s; height: 250px; margin-bottom: 0px;">
									<a onclick="showGame(1)">
										<img class="img-responsive" src="images/eiei.png" alt="img01" style="height: 170px; cursor: pointer;"/>
									</a>
									<div id="openCanvas1" class="og-expander" style="transition: height 350ms ease 0s; height: 590px;">
										<div class="og-expander-inner">
											<span onclick="closeGame(1)" class="og-close"></span>
											<div id="myfilterBC" style="position: absolute; background-color: rgb(0, 0, 0); opacity: 0.3; width: 1030px; height: 401px; display: none;"></div>
											<div id="restartGameBC" style="position: absolute; padding-top: 190px; padding-left: 500px; display: none;"><button onclick="restartGameBC()">Restart</button></div>
											<div id="canvascontainerBC"></div>
									
											<div class="col-12" style="text-align: center; font-size: 26px;">
												<p style="margin-bottom: 0px;"><b>BEST SCORE : </b><span id="bestScore1">0</span></p>
											</div>
									
											<div class="col-12" style="text-align: center;">
												<span style="margin-left: 10px; margin-right: 10px;">
													<i id="likeBC" style="font-size: 36px; cursor: pointer;" class="fa fa-thumbs-o-up" status="like" onclick="fellGameBC('nice',1)"></i>
													<span style="font-size: 25px; margin-left: 5px; margin-right: 10px;" id="numLike1">0</span>
												</span>
												<span style="margin-left: 10px; margin-right: 10px;">
													<i id="dislikeBC" style="font-size: 36px; cursor: pointer;" class="fa fa-thumbs-o-down" status="dislike" onclick="fellGameBC('bad',1)"></i>
													<span style="font-size: 25px; margin-left: 5px; margin-right: 10px;" id="numDislike1">0</span>
												</span>
											
												<input id="sc" type="hidden" name="score" value="280">
											</div>
									
											<div class="col-12" style="text-align: center; margin-top: 3px;">
												<a href="commentBC.html" style="border: 3px solid #333; padding: 10px 20px; letter-spacing: 2px; font-size: 16px; font-weight: 700; text-decoration: none; outline: none; color: #333;">
													<i class="fa fa-comments-o" style="font-size:30px"></i>
													<span style="font-size: 25px;">COMMENT</span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li id="li2" style="transition: height 350ms ease 0s; height: 250px; margin-bottom: 0px;">
									<a onclick="showGame(2)">
										<img class="img-responsive" src="images/fo.png" alt="img02" style="height: 170px; cursor: pointer;"/>
									</a>
									<div id="openCanvas2" class="og-expander" style="transition: height 350ms ease 0s; height: 590px;">
										<div class="og-expander-inner">
											<span onclick="closeGame(2)" class="og-close"></span>
											<div id="myfilterY" style="position: absolute; background-color: rgb(0, 0, 0); opacity: 0.3; width: 1030px; height: 401px; display: none;"></div>
											<div id="restartGameY" style="position: absolute; padding-top: 190px; padding-left: 500px; display: none;"><button onclick="restartGameY()">Restart</button></div>
											<div id="canvascontainerY"></div>
									
											<div class="col-12" style="text-align: center; font-size: 26px;">
												<p style="margin-bottom: 0px;"><b>BEST SCORE : </b><span id="bestScore2">0</span></p>
											</div>
									
											<div class="col-12" style="text-align: center;">
												<span style="margin-left: 10px; margin-right: 10px;">
													<i id="likeY" style="font-size: 36px; cursor: pointer;" class="fa fa-thumbs-o-up" status="like" onclick="fellGameY('nice',2)"></i>
													<span style="font-size: 25px; margin-left: 5px; margin-right: 10px;" id="numLike2">0</span>
												</span>
												<span style="margin-left: 10px; margin-right: 10px;">
													<i id="dislikeY" style="font-size: 36px; cursor: pointer;" class="fa fa-thumbs-o-down" status="dislike" onclick="fellGameY('bad',2)"></i>
													<span style="font-size: 25px; margin-left: 5px; margin-right: 10px;" id="numDislike2">0</span>
												</span>
											
												<input id="scY" type="hidden" name="score" value="280">
											</div>
									
											<div class="col-12" style="text-align: center; margin-top: 3px;">
												<a href="" style="border: 3px solid #333; padding: 10px 20px; letter-spacing: 2px; font-size: 16px; font-weight: 700; text-decoration: none; outline: none; color: #333;">
													<i class="fa fa-comments-o" style="font-size:30px"></i>
													<span style="font-size: 25px;">COMMENT</span>
												</a>
											</div> 
										</div>
									</div>
								</li>
								<li id="li3" style="transition: height 350ms ease 0s; height: 250px; margin-bottom: 0px;">
									<a onclick="showGame(3)">
										<img class="img-responsive" src="images/comingsoon.png" alt="img03" style="height: 170px; cursor: pointer;"/>
									</a>
									<div id="openCanvas3" class="og-expander" style="transition: height 350ms ease 0s; height: 500px;">
										<div class="og-expander-inner">
											<span onclick="closeGame(3)" class="og-close"></span>
											<div id="canvasGame3"></div>
										</div>
									</div>
								</li>
								<div class="clearfix"> </div>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="content-top" style="padding-top: 0px; padding-bottom: 0px;">
					<div class="wrap">	
						<div class="main">
							<ul id="og-grid" class="og-grid" style="padding-bottom: 0px; padding-top: 0px;">
								<li style="transition: height 350ms ease 0s; height: 120px; margin-top: 0px;">
									<div class="col-12">
										<h2><b>GAME : BLACK CAT</b></h2>
										<h3><b>SCORE</b></h3>
										<h3 id="showbestscore1"></h3>
										<h3><b id="gamerank1">YOU ARE RANK ...</b></h3>
									</div>
								</li>
								<li style="transition: height 350ms ease 0s; height: 120px; margin-top: 0px;">
									<div class="col-12">
										<h2><b>GAME : SPACE SHIP</b></h2>
										<h3><b>SCORE</b></h3>
										<h3 id="showbestscore2"></h3>
										<h3><b id="gamerank2">YOU ARE RANK ...</b></h3>
									</div>
								</li>
								<li style="transition: height 350ms ease 0s; height: 120px; margin-top: 0px;">
									<div class="col-12">
										<h2><b>GAME : ?????</b></h2>
										<h3><b>SCORE</b></h3>
										<h3>?????</h3>
										<h3><b>YOU ARE RANK #???</b></h3>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="content-top" tyle="padding-top: 0px;">
					<h2 class="new">RANK TOP 10</h2>
					<div class="wrap">	
						<div class="main">
							<ul id="og-grid" class="og-grid">
								<li style="transition: height 350ms ease 0s; height: 120px; margin-top: 0px;">
									<div class="col-12">
										<h2><b>GAME : BLACK CAT</b></h2>
										<h4>
											<table id="t1"></table>
										</h4>
									</div>
								</li>
								<li style="transition: height 350ms ease 0s; height: 120px; margin-top: 0px;">
									<div class="col-12">
										<h2><b>GAME : BLACK CAT</b></h2>
										<h4>
											<table id="t2"></table>
										</h4>
									</div>
								</li>
								<li style="transition: height 350ms ease 0s; height: 120px; margin-top: 0px;">
									<div class="col-12">
										<h2><b>GAME : ?????</b></h2>
										<h4>
											<table>
												<tr>
													<th style="width: 30px;"></th>
													<th style="width: 50px;">ID</th>
													<th style="width: 100px;">PLAYER</th>
													<th style="width: 80px;">SCORE</th>
												</tr>
												<tr>
													<td>1</td>
													<td>??????</td>
													<td>??????</td>
													<td>??????</td>
												</tr>
												<tr>
													<td>2</td>
													<td>??????</td>
													<td>??????</td>
													<td>??????</td>
												</tr>
												<tr>
													<td>3</td>
													<td>??????</td>
													<td>??????</td>
													<td>??????</td>
												</tr>
												<tr>
													<td>4</td>
													<td>??????</td>
													<td>??????</td>
													<td>??????</td>
												</tr>
												<tr>
													<td>5</td>
													<td>??????</td>
													<td>??????</td>
													<td>??????</td>
												</tr>
												<tr>
													<td>6</td>
													<td>??????</td>
													<td>??????</td>
													<td>??????</td>
												</tr>
												<tr>
													<td>7</td>
													<td>??????</td>
													<td>??????</td>
													<td>??????</td>
												</tr>
												<tr>
													<td>8</td>
													<td>??????</td>
													<td>??????</td>
													<td>??????</td>
												</tr>
												<tr>
													<td>9</td>
													<td>??????</td>
													<td>??????</td>
													<td>??????</td>
												</tr>
												<tr>
													<td>10</td>
													<td>??????</td>
													<td>??????</td>
													<td>??????</td>
												</tr>
											</table>
										</h4>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

		<script>
			getUser();
			showLikeDislike(document.getElementById("uemail").innerHTML);
			showAllLikeDislike(1);
			showAllLikeDislike(2);
			showRank10(1);
			showRank10(2);
			showUserRank(1);
			showUserRank(2);
			showUserScore(1);
			showUserScore(2);

			function getUser()
			{
				var name = '<?php echo $_REQUEST["uemail"]; ?>';
				console.log("name"+name);
				document.getElementById("uemail").innerHTML = name;
			}

			function updateScore(scoreCurrent,user,game){
				$.ajax({
					url : "addscore.php",
					async : true,
					data : 
						{ 
							uscore: scoreCurrent,
							uid : user,
							gid : game,
						},
					cache : false,
					type : "POST",
					success : function(result){
						console.log(result);
						showRank10(game);
						showUserRank(game);
						showUserScore(game);
					},
					error : function(result){
						console.log(result);
					}
				});
			}

			function queryBestScore(scoreCurrent,user,game)
			{
				$.ajax({
					url : "score.php",
					async : true,
					data : 
						{ 
							uscore: scoreCurrent,
							uid : user,
							gid : game,
						},
					cache : false,
					type : "POST",
					success : function(result){
						// console.log("best"+result);
						if(scoreCurrent > result)
						{
							var id = "bestScore"+game;
							updateScore(scoreCurrent,user,game);
							document.getElementById(id).innerHTML = scoreCurrent;
							
						}
						else
						{
							var id = "bestScore"+game
							document.getElementById(id).innerHTML = result;
						}
						
					},
					error : function(result)
					{
						console.log(result);
					}
					
				});
				
			}

			function likeOrDislike(act,user,game)
			{
				$.ajax({
					url : "actionLikeDislike.php",
					async : true,
					data : 
						{ 
							action : act,
							uid : user,
							gid : 'game'+game,
						},
					cache : false,
					type : "POST",
					success : function(result){
						console.log(result);
					},
					error : function(result)
					{
						console.log("error = " + result);
						showAllLikeDislike(game);
					}
				});
			}

			function showLikeDislike(user,)
			{
				$.ajax({
					url : "showLikeDislike.php",
					async : true,
					data : 
						{ 
							uid : user,
						},
					cache : false,
					type : "POST",
					success : function(result){
						for(var i = 0; i < result.length; i++)
						{
							// console.log(res)
							if(result[i].game1 == 1)
							{
								document.getElementById("likeBC").className = "fa fa-thumbs-up";
								document.getElementById("likeBC").setAttribute("status", "unlike");
								document.getElementById("dislikeBC").className = "fa fa-thumbs-o-down";
								document.getElementById("dislikeBC").setAttribute("status", "dislike");
							}
							if(result[i].game2 == 1)
							{
								document.getElementById("likeY").className = "fa fa-thumbs-up";
								document.getElementById("likeY").setAttribute("status", "unlike");
								document.getElementById("dislikeY").className = "fa fa-thumbs-o-down";
								document.getElementById("dislikeY").setAttribute("status", "dislike");
							}
							if(result[i].game1 == 2)
							{
								document.getElementById("dislikeBC").className = "fa fa-thumbs-down";
								document.getElementById("dislikeBC").setAttribute("status", "undislike");
								document.getElementById("likeBC").className = "fa fa-thumbs-o-up";
								document.getElementById("likeBC").setAttribute("status", "like");
							}
							if(result[i].game2 == 2)
							{
								document.getElementById("dislikeY").className = "fa fa-thumbs-down";
								document.getElementById("dislikeY").setAttribute("status", "undislike");
								document.getElementById("likeY").className = "fa fa-thumbs-o-up";
								document.getElementById("likeY").setAttribute("status", "like");
							}
							if(result[i].game1 == 0 && result[i].game2 == 0)
							{
								document.getElementById("dislikeBC").className = "fa fa-thumbs-o-down";
								document.getElementById("dislikeBC").setAttribute("status", "dislike");
								document.getElementById("dislikeY").className = "fa fa-thumbs-o-down";
								document.getElementById("dislikeY").setAttribute("status", "dislike");
							}
						}
					}
				});
			}

			function showAllLikeDislike(game)
			{
				$.ajax({
					url : "showAllLikeDislike.php",
					async : true,
					data : 
						{ 
							gid : 'game'+game
						},
					cache : false,
					type : "POST",
					success : function(result){
						for(var i = 0; i < result.length; i++)
						{
							document.getElementById("numLike"+game).innerHTML = result[i].like;
						
							document.getElementById("numDislike"+game).innerHTML = result[i].dislike;
						}
					}
				});
			}

			function showRank10(game)
			{
				$.ajax({
					url : "showRank10.php",
					async : true,
					data : 
						{ 
							gid : game
						},
					cache : false,
					type : "POST",
					success : function(result){
						var t = '<tr><th style="width: 30px;"></th><th style="width: 50px;">ID</th><th style="width: 100px;">PLAYER</th><th style="width: 80px;">SCORE</th></tr>';
						if(result.length < 10)
						{
							for(var i = 0; i < result.length; i++)
							{
								t += '<tr><td>'+(i+1)+'</td><td>'+result[i].user+'</td><td>'+result[i].user+'</td><td>'+result[i].score+'</td></tr>';
							}
						}
						else
						{
							for(var i = 0; i < 10; i++)
							{
								t += '<tr><td>'+(i+1)+'</td><td>'+result[i].user+'</td><td>'+result[i].user+'</td><td>'+result[i].score+'</td></tr>';
							}
						}
						document.getElementById("t"+game).innerHTML = t;
					},
					error : function(result){
						console.log("error" + result);
					}
				});
			}

			function showUserRank(game)
			{
				$.ajax({
					url : "showRank10.php",
					async : true,
					data : 
						{ 
							gid : game
						},
					cache : false,
					type : "POST",
					success : function(result){
						
						for(var i = 0; i < result.length; i++)
						{
							if(result[i].user == document.getElementById("uemail").innerHTML)
							{
								document.getElementById("gamerank"+game).innerHTML = "YOU ARE RANK #"+(i+1);
							}
						}

						// document.getElementById("t"+game).innerHTML = t;
					},
					error : function(result){
						console.log("error" + result);
					}
				});
			}

			function showUserScore(game)
			{
				$.ajax({
					url : "userScore.php",
					async : true,
					data : 
						{ 
							gid : game,
							uid : document.getElementById("uemail").innerHTML,
						},
					cache : false,
					type : "POST",
					success : function(result){
						console.log(result);
						document.getElementById("showbestscore"+game).innerHTML = result;
						
					},
					error : function(result){
						console.log("error" + result);
					}
				});
			}

		</script>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

		<script>
			//GAME BLACK CAT
			var myGameAreaBC;
			var myGamePieceBC;
			var myObstaclesBC = [];
			var myscoreBC;

			function restartGameBC() {
			document.getElementById("myfilterBC").style.display = "none";
			document.getElementById("restartGameBC").style.display = "none";
			myGameAreaBC.stop();
			myGameAreaBC.clear();
			myGameAreaBC = {};
			myGamePieceBC = {};
			myObstaclesBC = [];
			myscoreBC = {};
			document.getElementById("canvascontainerBC").innerHTML = "";
			startGameBC()
			}

			function startGameBC() {
				myGameAreaBC = new gameareaBC();
				myGamePieceBC = new componentBC(30, 30, "images/eiei.png", 10, 75, "image");
				myscoreBC = new componentBC("15px", "Consolas", "black", 220, 25, "text");
				myGameAreaBC.start();
			}

			function gameareaBC() {
				this.canvas = document.createElement("canvas");
				this.canvas.width = 320;
				this.canvas.height = 180;    
				document.getElementById("canvascontainerBC").appendChild(this.canvas);
				this.context = this.canvas.getContext("2d");
				this.pause = false;
				this.frameNo = 0;
				this.start = function() {
					this.interval = setInterval(updateGameAreaBC, 20);
					
					window.addEventListener('keydown', function (e) {
								myGameAreaBC.keys = (myGameAreaBC.keys || []);
								myGameAreaBC.keys[e.keyCode] = (e.type == "keydown");
							})
							window.addEventListener('keyup', function (e) {
								myGameAreaBC.keys[e.keyCode] = (e.type == "keydown");            
							})
				}
				this.stop = function() {
					clearInterval(this.interval);
					this.pause = true;
				}
				this.clear = function(){
					this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
				}
			}

			function componentBC(width, height, color, x, y, type) {

				this.type = type;
				if (type == "image") {
					this.image = new Image();
					this.image.src = color;
				}
				this.score = 0;    
				this.width = width;
				this.height = height;
				this.speedX = 0;
				this.speedY = 0;    
				this.x = x;
				this.y = y;    
				this.update = function() {
					ctx = myGameAreaBC.context;
					if (this.type == "text") {
						ctx.font = this.width + " " + this.height;
						ctx.fillStyle = color;
						ctx.fillText(this.text, this.x, this.y);
					} 
					if (this.type == "image") {
						ctx.drawImage(
							this.image, 
							this.x, 
							this.y,
							this.width, 
							this.height);
					}
					else {
						ctx.fillStyle = color;
						ctx.fillRect(this.x, this.y, this.width, this.height);
					}
				}
				this.crashWith = function(otherobj) {
					var myleft = this.x;
					var myright = this.x + (this.width);
					var mytop = this.y;
					var mybottom = this.y + (this.height);
					var otherleft = otherobj.x;
					var otherright = otherobj.x + (otherobj.width);
					var othertop = otherobj.y;
					var otherbottom = otherobj.y + (otherobj.height);
					var crash = true;
					if ((mybottom < othertop) || (mytop > otherbottom) || (myright < otherleft) || (myleft > otherright)) {
						crash = false;
					}
					return crash;
				}
			}

			function updateGameAreaBC() {
				var x, y, min, max, height, gap;
				for (i = 0; i < myObstaclesBC.length; i += 1) {
					if (myGamePieceBC.crashWith(myObstaclesBC[i])) {
						// upScore();
						queryBestScore(myscoreBC.score,document.getElementById("uemail").innerHTML,1);
						myGameAreaBC.stop();
						document.getElementById("myfilterBC").style.display = "block";
						document.getElementById("restartGameBC").style.display = "block";
						return;
					} 
				}
				if (myGameAreaBC.pause == false) {
					myGameAreaBC.clear();
					myGameAreaBC.frameNo += 1;
					myscoreBC.score +=1;        
					if (myGameAreaBC.frameNo == 1 || everyintervalBC(150)) {
						x = myGameAreaBC.canvas.width;
						y = myGameAreaBC.canvas.height - 100;
						min = 20;
						max = 100;
						height = Math.floor(Math.random()*(max-min+1)+min);
						min = 50;
						max = 100;
						gap = Math.floor(Math.random()*(max-min+1)+min);
						myObstaclesBC.push(new componentBC(10, height, "images/pap.png", x, 0, "image"));
						myObstaclesBC.push(new componentBC(10, x - height - gap, "images/pap.png", x, height + gap , "image"));
					}
					for (i = 0; i < myObstaclesBC.length; i += 1) {
						myObstaclesBC[i].x += -1;
						myObstaclesBC[i].update();
					}
					myscoreBC.text="SCORE: " + myscoreBC.score;  
					myscoreBC.update();
					scoreBC(myscoreBC.score);
					
					myGamePieceBC.x += myGamePieceBC.speedX;
					myGamePieceBC.y += myGamePieceBC.speedY;    
					myGamePieceBC.update();
					
					myGamePieceBC.speedX = 0;
					myGamePieceBC.speedY = 0; 
					
					if (myGameAreaBC.keys && myGameAreaBC.keys[65]) {myGamePieceBC.speedX = -3; }
					if (myGameAreaBC.keys && myGameAreaBC.keys[68]) {myGamePieceBC.speedX = 3; }
					if (myGameAreaBC.keys && myGameAreaBC.keys[87]) {myGamePieceBC.speedY = -3; }
					if (myGameAreaBC.keys && myGameAreaBC.keys[83]) {myGamePieceBC.speedY = 3; }
				}
			}
			function scoreBC(x) {
				document.getElementById("sc").value = x;
			}

			function everyintervalBC(n) {
				if ((myGameAreaBC.frameNo / n) % 0.5 == 0) {return true;}
				return false;
			}


			function clearmoveBC(e) {
				myGamePieceBC.speedX = 0; 
				myGamePieceBC.speedY = 0; 
			}
			startGameBC();

		</script>
		
		
		<script>
		//GAME SPACE SHIP
			var Object_player;
			var Object_Bonus = [];
			var myEnermy = [];
			var Text_Score;
			var _Score = 0;

			function restartGameY() {
				document.getElementById("myfilterY").style.display = "none";
				document.getElementById("restartGameY").style.display = "none";
				myGameAreaY.stop();
				myGameAreaY.clear();
				// myGameAreaY = {};
				Object_player = {};
				myEnermy = [];
				Object_Bonus = [];
				myscore = {};
				document.getElementById("canvascontainerY").innerHTML = "";
				startGameY()
				}

			function startGameY() {
				Object_player = new componentY(40, 40, "images/fo.png", 240, 400, "image");
				myScore = new componentY("30px", "Consolas", "black", 800, 40, "text");
				myGameAreaY.start();
			}

			var myGameAreaY = {
				canvas : document.createElement("canvas"),
				start : function() {
					this.canvas.width = 1051;
					this.canvas.height = 400;
					this.context = this.canvas.getContext("2d");
					document.getElementById("canvascontainerY").appendChild(this.canvas);
					this.frameNo = 0;
					this.interval = setInterval(updateGameAreaY, 20);
				},
				clear : function() {
					this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
				},
				stop : function() {
					clearInterval(this.interval);
				}
			}

			function componentY(width, height, color, x, y, type) {
				this.type = type;
				if (type == "image") {
					this.image = new Image();
					this.image.src = color;
				}
				this.width = width;
				this.height = height;
				this.speedX = 0;
				this.speedY = 0;
				this.score = 0;
				this.x = x;
				this.y = y;    
				this.update = function() {
					ctx = myGameAreaY.context;
					if (this.type == "text") {
						ctx.font = this.width + " " + this.height;
						ctx.fillStyle = color;
						ctx.fillText(this.text, this.x, this.y);
					} 
					if (type == "image") {
						ctx.drawImage(this.image, 
							this.x, 
							this.y,
							this.width, this.height);
					}
					else {
						ctx.fillStyle = color;
						ctx.fillRect(this.x, this.y, this.width, this.height);
					}
					$('body').on('keydown',function(e){
						if(e.keyCode == 37 )
						{
							Object_player.speedX = -6;
						}
					});

					$('body').on('keydown',function(e){
						if(e.keyCode == 39 )
						{
							Object_player.speedX = 6;
						}
					});
				}
				this.newPos = function() {
					this.x += this.speedX;
					this.y += this.speedY;    
					this.hitBottom(); 
					this.hitRight(); 
					this.hitLeft();
				}
				this.hitRight = function() {
					var rockbottom = myGameAreaY.canvas.width - this.width;
					if (this.x > rockbottom) {
						this.x = rockbottom;
						this.gravitySpeed = 0;
					}
				}    
				this.hitLeft = function() {
					var rockbottom = 0;
					if (this.x < rockbottom) {
						this.x = rockbottom;
						this.gravitySpeed = 0;
					}
				}    
				this.hitBottom = function() {
					var rockbottom = myGameAreaY.canvas.height - this.height;
					if (this.y > rockbottom) {
						this.y = rockbottom;
						this.gravitySpeed = 0;
					}
				}
				this.crashWith = function(otherobj,i) {
					var myleft = this.x;
					var myright = this.x + (this.width);
					var mytop = this.y;
					var mybottom = this.y + (this.height);
					var otherleft = otherobj.x;
					var otherright = otherobj.x + (otherobj.width);
					var othertop = otherobj.y;
					var otherbottom = otherobj.y + (otherobj.height);
					var bottom  = myGameAreaY.canvas.height - this.height;
					var crash = true;
					if ((mybottom < othertop) || (mytop > otherbottom) || (myright < otherleft) || (myleft > otherright)) {
						crash = false;
					}
					else
					{
						_Score += 5;
						crash = false;
						Object_Bonus.splice(i,1);
					}
					return crash;
				}
				this.crash = function(otherobj,i) {
					var myleft = this.x;
					var myright = this.x + (this.width);
					var mytop = this.y;
					var mybottom = this.y + (this.height);
					var otherleft = otherobj.x;
					var otherright = otherobj.x + (otherobj.width);
					var othertop = otherobj.y;
					var otherbottom = otherobj.y + (otherobj.height);
					var bottom  = myGameAreaY.canvas.height - this.height;
					var crash = true;
					if ((mybottom < othertop) || (mytop > otherbottom) || (myright < otherleft) || (myleft > otherright)) {
						crash = false;
					}
					return crash;
				}   
			}

			function updateGameAreaY() {
				var x;
				var x2;
				for (i = 0; i < Object_Bonus.length; i += 1) {
					if (Object_player.crashWith(Object_Bonus[i],i)) {
						return;
					} 
				}

				for (i = 0; i < myEnermy.length; i += 1) {
					if (Object_player.crash(myEnermy[i],i)) {
						myGameAreaY.stop();
						queryBestScore(_Score,document.getElementById("uemail").innerHTML,2);
						document.getElementById("myfilterY").style.display = "block";
						document.getElementById("restartGameY").style.display = "block";
						return;
					} 
				}

				myGameAreaY.clear();
				myGameAreaY.frameNo += 1;

				//myObstacles
				if (myGameAreaY.frameNo == 1 || everyintervalY(80)) {
					x = 1 + Math.floor(Math.random() * 1035);

					Object_Bonus.push(new componentY(30, 30, "images/Coin.png", x, 0 , "image"));
				}
				for (i = 0; i < Object_Bonus.length; i += 1) {
					Object_Bonus[i].y += 2;
					Object_Bonus[i].update();
				}
				//myEnermy
				if (everyintervalY(50)) {
					x2 = 1 + Math.floor(Math.random() * 1035);

					myEnermy.push(new componentY(50, 50, "images/Meteorit.png", x2, 0 , "image"));
				}
				for (i = 0; i < myEnermy.length; i += 1) {
					myEnermy[i].y += 3;
					myEnermy[i].update();
				}

				Object_player.newPos();    
				Object_player.update();
				//myObstacle.update();
				myScore.text="SCORE: " + _Score;
				myScore.update();
			}

			function everyintervalY(n) {
				if ((myGameAreaY.frameNo / n) % 1 == 0) {return true;}
				return false;
			}
			startGameY();

		</script>

		<script>
		
			function showGame(x) {
				if(x == 1) {
					var add = document.getElementById("li1");
					add.classList.add("og-expanded");
					
					var remove = document.getElementById("li2");
					remove.classList.remove("og-expanded");
					
					var remove = document.getElementById("li3");
					remove.classList.remove("og-expanded");
					
					document.getElementById("openCanvas1").style.display = "block";
					document.getElementById("openCanvas2").style.display = "none";
					document.getElementById("openCanvas3").style.display = "none";
					
					document.getElementById("li1").style.height = "760px";
					
				}
				
				if(x == 2) {
					var remove = document.getElementById("li1");
					remove.classList.remove("og-expanded");
					
					var add = document.getElementById("li2");
					add.classList.add("og-expanded");
					
					var remove = document.getElementById("li3");
					remove.classList.remove("og-expanded");
					
					document.getElementById("openCanvas1").style.display = "none";
					document.getElementById("openCanvas2").style.display = "block";
					document.getElementById("openCanvas3").style.display = "none";
					
					document.getElementById("li2").style.height = "760px";
					
				
				}
				
				if(x == 3) {
					
					var remove = document.getElementById("li1");
					remove.classList.remove("og-expanded");
					
					var remove = document.getElementById("li2");
					remove.classList.remove("og-expanded");
					
					var add = document.getElementById("li3");
					add.classList.add("og-expanded");
					
					document.getElementById("openCanvas1").style.display = "none";
					document.getElementById("openCanvas2").style.display = "none";
					document.getElementById("openCanvas3").style.display = "block";
					
					document.getElementById("li3").style.height = "760px";
				}
			}
			
			function closeGame(x) {
				if(x == 1) {
					var remove = document.getElementById("li1");
					remove.classList.remove("og-expanded");
					
					var remove = document.getElementById("li2");
					remove.classList.remove("og-expanded");
					
					var remove = document.getElementById("li3");
					remove.classList.remove("og-expanded");
					
					document.getElementById("openCanvas1").style.display = "none";
					document.getElementById("openCanvas2").style.display = "none";
					document.getElementById("openCanvas3").style.display = "none";
					
					document.getElementById("li1").style.height = "250px";
					document.getElementById("li2").style.height = "250px";
					document.getElementById("li3").style.height = "250px";
				}
				
				if(x == 2) {
					var remove = document.getElementById("li1");
					remove.classList.remove("og-expanded");
					
					var remove = document.getElementById("li2");
					remove.classList.remove("og-expanded");
					
					var remove = document.getElementById("li3");
					remove.classList.remove("og-expanded");
					
					document.getElementById("openCanvas1").style.display = "none";
					document.getElementById("openCanvas2").style.display = "none";
					document.getElementById("openCanvas3").style.display = "none";
					
					document.getElementById("li1").style.height = "250px";
					document.getElementById("li2").style.height = "250px";
					document.getElementById("li3").style.height = "250px";
				}
				
				if(x == 3) {
					var remove = document.getElementById("li1");
					remove.classList.remove("og-expanded");
					
					var remove = document.getElementById("li2");
					remove.classList.remove("og-expanded");
					
					var remove = document.getElementById("li3");
					remove.classList.remove("og-expanded");
					
					document.getElementById("openCanvas1").style.display = "none";
					document.getElementById("openCanvas2").style.display = "none";
					document.getElementById("openCanvas3").style.display = "none";
					
					document.getElementById("li1").style.height = "250px";
					document.getElementById("li2").style.height = "250px";
					document.getElementById("li3").style.height = "250px";
				}
			}
			
			function fellGameBC(fell,game) {
				
				if(fell == 'nice') {
					var x = document.getElementById("likeBC").getAttribute("status"); 
					if(x == "like") {
						document.getElementById("likeBC").className = "fa fa-thumbs-up";
						document.getElementById("likeBC").setAttribute("status", "unlike");
						document.getElementById("dislikeBC").className = "fa fa-thumbs-o-down";
						document.getElementById("dislikeBC").setAttribute("status", "dislike");
						likeOrDislike(1,document.getElementById("uemail").innerHTML,game);
					}
					if(x == "unlike") {
						document.getElementById("likeBC").className = "fa fa-thumbs-o-up";
						document.getElementById("likeBC").setAttribute("status", "like");
						likeOrDislike(0,document.getElementById("uemail").innerHTML,game);
					}
				}
				if(fell == 'bad') {
					var y = document.getElementById("dislikeBC").getAttribute("status"); 
					if(y == "dislike") {
						document.getElementById("dislikeBC").className = "fa fa-thumbs-down";
						document.getElementById("dislikeBC").setAttribute("status", "undislike");
						document.getElementById("likeBC").className = "fa fa-thumbs-o-up";
						document.getElementById("likeBC").setAttribute("status", "like");
						likeOrDislike(2,document.getElementById("uemail").innerHTML,game);
					}
					if(y == "undislike") {
						document.getElementById("dislikeBC").className = "fa fa-thumbs-o-down";
						document.getElementById("dislikeBC").setAttribute("status", "dislike");
						likeOrDislike(0,document.getElementById("uemail").innerHTML,game);
					}
				}
			}
			function fellGameY(fell,game) {
				
				if(fell == 'nice') {
					var x = document.getElementById("likeY").getAttribute("status"); 
					if(x == "like") {
						document.getElementById("likeY").className = "fa fa-thumbs-up";
						document.getElementById("likeY").setAttribute("status", "unlike");
						document.getElementById("dislikeY").className = "fa fa-thumbs-o-down";
						document.getElementById("dislikeY").setAttribute("status", "dislike");
						likeOrDislike(1,document.getElementById("uemail").innerHTML,game);
					}
					if(x == "unlike") {
						document.getElementById("likeY").className = "fa fa-thumbs-o-up";
						document.getElementById("likeY").setAttribute("status", "like");
						likeOrDislike(0,document.getElementById("uemail").innerHTML,game);
					}
				}
				if(fell == 'bad') {
					var y = document.getElementById("dislikeY").getAttribute("status"); 
					if(y == "dislike") {
						document.getElementById("dislikeY").className = "fa fa-thumbs-down";
						document.getElementById("dislikeY").setAttribute("status", "undislike");
						document.getElementById("likeY").className = "fa fa-thumbs-o-up";
						document.getElementById("likeY").setAttribute("status", "like");
						likeOrDislike(2,document.getElementById("uemail").innerHTML,game);
					}
					if(y == "undislike") {
						document.getElementById("dislikeY").className = "fa fa-thumbs-o-down";
						document.getElementById("dislikeY").setAttribute("status", "dislike");
						likeOrDislike(0,document.getElementById("uemail").innerHTML,game);
					}
				}
			}

		</script>	
	
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	
		<div class="footer" style="margin-top: 50px;">
			<div class="footer-middle">
				<div class="container">
					<div class="footer-middle-in">
						<h6>About us</h6>
						<p>Suspendisse sed accumsan risus. Curabitur rhoncus, elit vel tincidunt elementum, nunc urna tristique nisi, in interdum libero magna tristique ante. adipiscing varius. Vestibulum dolor lorem.</p>
					</div>
					<div class="footer-middle-in">
						<h6>Information</h6>
						<ul>
							<li><a href="#">About Us</a></li>
							<li><a href="#">Delivery Information</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms & Conditions</a></li>
						</ul>
					</div>
					<div class="footer-middle-in">
						<h6>Customer Service</h6>
						<ul>
							<li><a href="contact.html">Contact Us</a></li>
							<li><a href="#">Returns</a></li>
							<li><a href="contact.html">Site Map</a></li>
						</ul>
					</div>
					<div class="footer-middle-in">
						<h6>My Account</h6>
						<ul>
							<li><a href="#">Order History</a></li>
							<li><a href="#">Wish List</a></li>
							<li><a href="#">Newsletter</a></li>
						</ul>
					</div>
					<div class="footer-middle-in">
						<h6>Extras</h6>
						<ul>
							<li><a href="#">Affiliates</a></li>
							<li><a href="#">Specials</a></li>
						</ul>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>		
		</div>

	</body>
</html>
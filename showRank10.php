﻿<?php
    header('Content-Type: application/json');

    $GID = $_POST["gid"];

    $host = 'localhost'; //127.0.0.1
    $dbname = 'my_db';
    $user = 'root';
    $pass = '';
    $conn = mysqli_connect($host,$user,$pass);

    mysqli_select_db($conn,$dbname);

    $sql = "SELECT score , (@curRank := @curRank + 1) AS rankk, userID FROM scoreuser p, (SELECT @curRank := 0) r WHERE gameID = $GID ORDER BY score DESC;";
    $result = mysqli_query($conn,$sql);

    while( $row = mysqli_fetch_array($result))
    {
        $jsonData[] = array(
            "score" => $row["score"],
            "rank" => $row["rankk"],
            "user" => $row["userID"]
        );
    }  

    $json = json_encode($jsonData);
    echo $json;
?>